# Code challenge Register Page 

This is the code challenge To 'CREATE AN ACCOUNT' in automationpractice.com site.

# Installation

Install Selenium-webdriver using npm:
"npm install selenium-webdriver"

Install Jest using npm:
"npm install --save-dev jest"

Use "npm run test" to run.

## Usage

> Before moving with the test cases, we need to gather some requirements like:

- Gender
- First Name
- Last Name
- User Name
- Password
- Email ID
- Phone Number
- Address

> Selenium Form WebElement

- Text fields
- Buttons
- Radio buttons
- Checkboxe

