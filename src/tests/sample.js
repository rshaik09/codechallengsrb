const { withCapabilities, By, Key, until} = require("selenium-webdriver");
const chromeDriver = require("../drivers/chrome");
var wd = require('selenium-webdriver'),
    SeleniumServer = require('selenium-webdriver/remote').SeleniumServer,
    request = require('request');

describe("Aura Code Challenge - Create User Account Tests", () => {
 let driver;
  beforeAll(() => {
    driver = chromeDriver();
});

  afterAll(async () => {
    await driver.quit();
  });
  
  test("it loads authentication page", async () => {  
    
    await driver.get(
      "http://automationpractice.com/index.php?controller=authentication&back=my-account"
    );
     // Generating random number
    function getRandomInt(max) {
      return Math.floor(Math.random() * Math.floor(max));
    }
    
    // Constructing Email id with random number
    var randomnum = getRandomInt(5000);
    var testinstrname = "testerhyd";
    var testextension = "@gmail.com";
    var addname = testinstrname.concat(randomnum);
    var testingEmail = addname.concat(testextension);

    // Entering Email-ID
    await driver.findElement(wd.By.css("#email_create")).sendKeys(testingEmail);
	
    //Get page Title
    const title = await driver.getTitle();
    console.log(title);
    expect(title).toBe("Login - My Store");

    await driver.findElement(wd.By.css("#SubmitCreate")).click(); 
     
    //Personal Information Form

    await driver.findElement(By.xpath('(//input[@type="text"])[2]')).sendKeys("tester");
    await driver.findElement(By.xpath('(//input[@type="text"])[3]')).sendKeys("testing");
    await driver.findElement(wd.By.id("passwd")).sendKeys("mypassword");
    
    //Address form
      function sendByTextScroll(text) {

        companyelement = driver.findElement(By.xpath('(//input[@type="text"])[7]'));
        driver.executeScript("arguments[0].scrollIntoView()", companyelement);
        driver.sleep(300);
        companyelement.sendKeys("TSSS INFOTECH");

        addresselement = driver.findElement(By.xpath('(//input[@type="text"])[8]'));
        driver.executeScript("arguments[0].scrollIntoView()", addresselement);
        driver.sleep(300);
        addresselement.sendKeys("C/o TSSS INFOTECH,2-12,New avenue road");
        
        addressLine2element = driver.findElement(By.xpath('(//input[@type="text"])[9]'));
        driver.executeScript("arguments[0].scrollIntoView()", addressLine2element);
        driver.sleep(300);
        addressLine2element.sendKeys("Sri city, apparna buildings");

        cityelement = driver.findElement(By.xpath('(//input[@type="text"])[10]'));
        driver.executeScript("arguments[0].scrollIntoView()", cityelement);
        driver.sleep(300);
        cityelement.sendKeys("Bell Gardens");
        
        //State
        driver.findElement(By.xpath("//*[@id=\"id_state\"]")).click();
        driver.findElement(By.xpath("//*[@id=\"id_state\"]/option[@value=\"5\"]")).click();

        zipelement = driver.findElement(By.xpath('(//input[@type="text"])[11]'));
        driver.executeScript("arguments[0].scrollIntoView()", zipelement);
        driver.sleep(300);
        zipelement.sendKeys("90201");

        //Country
        driver.findElement(By.xpath("//*[@id=\"id_country\"]")).click();
        driver.findElement(By.xpath("//*[@id=\"id_country\"]/option[@value=\"21\"]")).click();

        addinfoelement = driver.findElement(By.xpath('//*[@id="other"]'));
        driver.executeScript("arguments[0].scrollIntoView()", addinfoelement);
        driver.sleep(300);
        addinfoelement.sendKeys("I'm fine with this, Thank you");
        
        mpelement = driver.findElement(By.xpath('(//input[@type="text"])[13]'));
        driver.executeScript("arguments[0].scrollIntoView()", mpelement);
        driver.sleep(300);
        mpelement.sendKeys("754-3010");
		
		//Submit button
        submit_btn = driver.findElement(wd.By.id("//*[@id=\"submitAccount\"]//span"))
        driver.executeScript("arguments[0].scrollIntoView()", submit_btn.click());

        
      }
    
  });
});